<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('category',[\App\Http\Controllers\Api\ApiCategoryController::class,'getCategory']);
Route::get('products',[\App\Http\Controllers\Api\ApiProductController::class,'getProduct']);
Route::get('product/details',[\App\Http\Controllers\Api\ApiProductController::class,'getProductDetails']);
