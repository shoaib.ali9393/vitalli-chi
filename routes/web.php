<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return view('auth/login');
});



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/user/list', [App\Http\Controllers\UserController::class, 'index'])->name('admin.users');


Route::get('/category/list', [App\Http\Controllers\CategoryController::class, 'index'])->name('admin.category');
Route::post('/category/add', [App\Http\Controllers\CategoryController::class, 'save'])->name('admin.add.category');
Route::get('ajax/category/data', [App\Http\Controllers\CategoryController::class, 'getCategory'])->name('ajax.category.data');
Route::post('update/category', [App\Http\Controllers\CategoryController::class, 'updateCategory'])->name('admin.update.category');
Route::post('delete/category', [App\Http\Controllers\CategoryController::class, 'deleteCategory'])->name('admin.delete.category');
Route::get('update/status/category/{id?}', [App\Http\Controllers\CategoryController::class, 'updateStatusCategory'])->name('admin.update.status.category');

Route::get('/products/list', [App\Http\Controllers\ProductController::class, 'index'])->name('admin.products');
Route::post('/product/add', [App\Http\Controllers\ProductController::class, 'save'])->name('admin.add.product');
Route::post('/delete/product', [App\Http\Controllers\ProductController::class, 'deleteProduct'])->name('admin.delete.product');
Route::get('update/status/product/{id?}', [App\Http\Controllers\ProductController::class, 'updateStatusProduct'])->name('admin.update.status.product');
Route::get('ajax/product/data', [App\Http\Controllers\ProductController::class, 'getProdct'])->name('ajax.product.data');
Route::post('update/product', [App\Http\Controllers\ProductController::class, 'updateProduct'])->name('admin.update.product');

Route::get('/admin/profile/settings', [App\Http\Controllers\Settings::class, 'index'])->name('admin.profile');
Route::post('/admin/profile/save', [App\Http\Controllers\Settings::class, 'save'])->name('admin.profile.save');
