<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const active="ACTIVE";
    const disabled="DISABLED";
    use HasFactory;
    protected $fillable=[
        'name',
        'category_id',
        'product_question',
        'product_answer',
        'unique_url',
        'description',
        'ingredients',
        'allergens',
        'shipping',
        'status',
        'banner_image',
        'price',
    ];
    public function categorys()
    {
        return $this->belongsTo(Category::class , 'category_id','id');
    }
//    By mistake Make productImages() don't use anymore
    public function productImages()
    {
        return $this->hasMany(ProductImages::class , 'product_id','id')
            ->select(['product_id','image']);
    }

    public function imagesproduct()
    {
        return $this->hasMany(ProductImages::class , 'product_id','id')
            ->select(['product_id','image']);
    }
}
