<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    const active="ACTIVE";
    const disabled="DISABLED";
    use HasFactory;
    protected $fillable=[
        'name',
        'banner_image',
        'status',
    ];

    public function products()
    {
        return $this->hasMany(Product::class , 'category_id','id');
    }
}
