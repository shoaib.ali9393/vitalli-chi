<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    function index()
    {
        $productData = Product::with(['productImages', 'categorys'])->get();
        return view('admin.product', compact('productData'));
    }

    function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_name' => 'required',
            'price' => 'required',
            'product_category' => 'required',
            'product_banner' => 'mimes:jpeg,jpg,png,gif|required',
            'product_image' => 'mimes:jpeg,jpg,png,gif|required',
        ]);
        if ($validator->fails()) {
            return Redirect()->back()->withErrors($validator)->withInput();
        }
        $productBannerImageName = "product_banner" . time() . rand() . '.' . $request->product_banner->extension();
        $productImageName = "productimage" . time() . rand() . '.' . $request->product_image->extension();
        $product = Product::create([
            'name' => $request->product_name,
            'price' => $request->price,
            'category_id' => $request->product_category,
            'banner_image' => $productBannerImageName,
            'product_question' => $request->product_question,
            'product_answer' => $request->product_answer,
            'description' => $request->product_description,
            'ingredients' => $request->product_ingredients,
            'allergens' => $request->product_allergens,
            'shipping' => $request->product_shipping,
        ]);
        $product->productImages()->create([
            'image' => $productImageName,
        ]);
        $request->product_banner->move(public_path('images/uploads'), $productBannerImageName);
        $request->product_image->move(public_path('images/uploads'), $productImageName);
        return redirect()->route('admin.products')->with('success', 'Product Added Successfully!');

    }

    function deleteProduct(Request $request)
    {
        $data = Product::with('productImages')->where('id', $request->id)->first();

        if (count($data->productImages) > 0) {
            foreach ($data->productImages as $pimages) {
                $file_path = public_path() . '/images/uploads/' . $pimages->image;
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }
        $file_path = public_path() . '/images/uploads/' . $data->banner_image;
        if (is_file($file_path)) {
            unlink($file_path);
        }

        $data->delete();
        return redirect()->route('admin.products')->with('success', 'Product Deleted Successfully!');

    }

    function updateStatusProduct(Request $request)
    {
        $data = Product::where('id', $request->id)->first();
        if ($data->status == Product::active) {
            $data->update(['status' => Product::disabled]);
        } else {
            $data->update(['status' => Product::active]);
        }
        $data->save();
        return redirect()->route('admin.products')->with('success', 'Product Status Update Successfully!');
    }

    function getProdct(Request $request)
    {
        $productData = Product::with(['productImages', 'categorys'])->where('id', $request->id)->first();
        return $productData;
    }

    function updateProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'edit_product_name' => 'required',
            'edit_price' => 'required',
            'edit_product_category' => 'required',
            'edit_product_banner' => 'mimes:jpeg,jpg,png,gif',
            'edit_product_image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        if ($validator->fails()) {
            return Redirect()->back()->withErrors($validator)->withInput();
        }

        $product = Product::with(['productImages'])->where('id', $request->id)->first();
        $product->update([
            'name' => $request->edit_product_name,
            'price' => $request->edit_price,
            'category_id' => $request->edit_product_category,
            'product_question' => $request->edit_product_question,
            'product_answer' => $request->edit_product_answer,
            'description' => $request->edit_product_description,
            'ingredients' => $request->edit_product_ingredients,
            'allergens' => $request->edit_product_allergens,
            'shipping' => $request->edit_product_shipping,
        ]);
        if ($request->edit_product_banner) {
            $productBannerImageName = "product_banner" . time() . rand() . '.' . $request->edit_product_banner->extension();
            $request->edit_product_banner->move(public_path('images/uploads'), $productBannerImageName);
            if($product->banner_image) {
                $file_path = public_path() . '/images/uploads/' . $product->banner_image;
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
                $product->update(['banner_image' => $productBannerImageName]);

        }
        if ($request->edit_product_image) {
            $productImageName = "product_image".time().rand().'.'.$request->edit_product_image->extension();
            $request->edit_product_image->move(public_path('images/uploads'), $productImageName);
            if(count($product->productImages) > 0 ) {
                $file_path = public_path() . '/images/uploads/' . $product->productImages[0]->image;
                if (is_file($file_path)) {
                    unlink($file_path);
                }
                $product->productImages[0]->update(['image' => $productImageName]);
            }
            {
                $product->productImages()->create([
                    'image' => $productImageName
                ]);
            }
            }
        $product->save();
        return redirect()->route('admin.products')->with('success', 'Product Updated Successfully!');
    }
}
