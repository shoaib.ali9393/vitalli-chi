<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductImages;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class ApiProductController extends Controller
{

    function getProduct()
    {
        $baseurl = URL::to('/');

        $data =Product::with('imagesproduct')->where("status",Product::active);
        if(\request()->category_id)
        {
            $data->where('category_id' , \request()->category_id);
        }
        if( isset(\request()->offset) && isset(\request()->limit)){
            $data->skip(\request()->offset)->take(\request()->limit);
        }
        $data = $data->get();
        foreach ($data as $dataproduct)
        {
            $dataproduct->banner_image =$baseurl.'/public/images/uploads/'.$dataproduct->banner_image;
            foreach ($dataproduct->imagesproduct as $pimages)
            {
                unset($pimages->product_id);
                $pimages->image =$baseurl.'/public/images/uploads/'.$pimages->image;
            }
        }
        return response()
            ->json(['status' => '200', 'success' => true, 'message' => 'Product Data!', 'data' => $data], 200);
    }

    function getProductDetails()
    {
        $baseurl = URL::to('/');
        $data =Product::with('imagesproduct');
        $data->where('id' , \request()->product_id);
        $data = $data->first();
            if($data != null) {
                $data->banner_image = $baseurl . '/public/images/uploads/' . $data->banner_image;
                foreach ($data->imagesproduct as $pimages) {
                    unset($pimages->product_id);
                    $pimages->image = $baseurl . '/public/images/uploads/' . $pimages->image;
                }
            }
        return response()
            ->json(['status' => '200', 'success' => true, 'message' => 'Product Details!', 'data' => $data], 200);
    }

}
