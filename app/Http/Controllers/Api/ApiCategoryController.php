<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class ApiCategoryController extends Controller
{
   function getCategory()
   {
       $baseurl = URL::to('/');
       $data =Category::where("status",Category::active);
       if(\request()->category_id)
       {
           $data->where('id' , \request()->category_id);
           $data = $data->first();
           $data->banner_image = $baseurl.'/public/images/uploads/'.$data->banner_image;
       }
       else{
           if( isset(\request()->offset) && isset(\request()->limit)){
               $data->skip(\request()->offset)->take(\request()->limit);
           }
           $data = $data->get();
          foreach ($data as $dataa)
          {
              $dataa->banner_image = $baseurl.'/public/images/uploads/'.$dataa->banner_image;
          }
       }
       return response()
           ->json(['status' => '200', 'success' => true, 'message' => 'Category Data!', 'data' => $data], 200);
   }
}
