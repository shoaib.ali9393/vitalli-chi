<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Testing\Fluent\Concerns\Has;

class Settings extends Controller
{
    function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        return view('admin.profile', compact('user'));
    }

    function save()
    {
        $validator = Validator::make(\request()->all(), [
            'name' => ['required', 'string',],
        ]);
        if ($validator->fails()) {
            return Redirect()->back()->withErrors($validator);
        }
        if (\request()->password) {
            $validator = Validator::make(\request()->all(), [
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
            if ($validator->fails()) {
                return Redirect()->back()->withErrors($validator);
            }
        }
        $user = User::where('id', \request()->user_id)->first();
        if (\request()->profile_image) {
            $imageName = time() . rand() . '.' . \request()->profile_image->extension();
            \request()->profile_image->move(public_path('/images/uploads/'), $imageName);
            if (isset($user->image) && $user->image != null) {
                $file_path = public_path() . '/images/uploads/' . $user->image;
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
            $user->update([
                'image' => $imageName,
            ]);
        }
        $user->update([
            'name' => isset(\request()->name) ? \request()->name : Null,
            'password' => Hash::make('password'),
        ]);
        $user->save();
        return redirect()->route('admin.profile')->with('success', 'Profile Updated!');

    }
}
