<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Validator;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    function index()
    {
        $data = Category::withCount('products')->get();
        return view('admin.category', compact('data'));
    }

    function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_image' => 'mimes:jpeg,jpg,png,gif|required',
            'category_name' => 'required|string|unique:categories,name',
        ]);
        if ($validator->fails()) {
            return Redirect()->back()->withErrors($validator)->withInput();
        }
        $imageName = time().rand().'.' . $request->category_image->extension();
        Category::create([
            'name' => $request->category_name,
            'banner_image' => $imageName,
        ]);
        $request->category_image->move(public_path('images/uploads'), $imageName);
        return redirect()->route('admin.category')->with('success', 'Category Added Successfully!');
    }

    function getCategory(Request $request)
    {
        $data = Category::where('id', $request->id)->first();
        return $data;
    }

    function updateCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'edit_category_image' => 'mimes:jpeg,jpg,png,gif',
            'edit_category_name' => 'required|string|unique:categories,name,' . $request->id . '',
        ]);
        if ($validator->fails()) {
            return Redirect()->back()->withErrors($validator)->withInput();
        }
        $old_data = Category::where('id', $request->id)->first();
        $old_data->update([
            'name' => $request->edit_category_name,
        ]);
        if ($request->edit_category_image) {
            $imageName = time().rand().'.' . $request->edit_category_image->extension();
            $request->edit_category_image->move(public_path('/images/uploads/'), $imageName);
            if($old_data->banner_image) {
                $file_path = public_path() . '/images/uploads/' . $old_data->banner_image;
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
            $old_data->update([
                'banner_image' => $imageName,
            ]);
        }
        $old_data->save();
        return redirect()->route('admin.category')->with('success', 'Category Updated Successfully!');
    }

    function deleteCategory(Request $request)
    {
        $data = Category::with('products.productImages')->where('id', $request->id)->first();
        if (count($data->products) > 0) {
            foreach ($data->products as $product) {
                if (count($product->productImages) > 0) {
                    foreach ($product->productImages as $pimages) {
                        $file_path = public_path() . '/images/uploads/' . $pimages->image;
                        if (is_file($file_path)) {
                            unlink($file_path);
                        }
                    }
                }
                $file_path = public_path() . '/images/uploads/' . $product->banner_image;
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }
        $file_path = public_path() . '/images/uploads/' . $data->banner_image;
        if (is_file($file_path)) {
            unlink($file_path);
        }
        $data->delete();
        return redirect()->route('admin.category')->with('success', 'Category Deleted Successfully!');

    }

    function updateStatusCategory(Request $request)
    {
         $data=Category::where('id',$request->id)->first();
         if($data->status == Product::active){
             $data->update(['status' => Category::disabled]);
         }
         else{
             $data->update(['status' => Category::active]);
         }
         $data->save();
        return redirect()->route('admin.category')->with('success', 'Category Status Update Successfully!');
    }
}
