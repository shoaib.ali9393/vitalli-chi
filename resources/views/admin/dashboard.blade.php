@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="container">
            <div class="row my-5">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <h5 class="fw-bold">Hi Admin,</h5>
                    <span class="text-secondary">Welcome to Dashboard</span>
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="d-flex justify-content-end mt-2">
                        <button type="button" class="btn btn-primary fw-bold rounded" data-bs-toggle="modal"
                                href="#exampleModalToggle" role="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                                 class="bi bi-plus-lg" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                      d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                            </svg>
                            &nbsp;Add Product
                        </button>
                    </div>
                </div>
            </div>

            <div class="row gx-3 gx-xxl-5 gy-4">
                <!-- Canvas Js Chart Start -->
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="p-3  bg-white">
                        <div id="chartContainer" style="height: 330px; width: 100%;"></div>
                    </div>
                </div>
                <!-- Canvas Js Chart End -->
                <!-- 1st Section Summary Start -->
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="p-3 bg-white">
                        <!-- -------------  -->
                        <div class="row">
                            <div class="col">
                                <h6 class="text-heading_s">Summmary</h6>
                                <span class="text-date_s">March 2022</span>
                            </div>
                            <div class="col d-flex justify-content-end">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                     class="bi bi-three-dots" viewBox="0 0 16 16">
                                    <path
                                        d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z"/>
                                </svg>
                            </div>
                        </div>
                        <!-- -------------  -->
                        <div class="row my-4 ms-5">
                            <div class="col">
                                <h6 class="text-count_s">219</h6>
                                <span class="text-category_s">Users</span>
                            </div>
                            <div class="col">
                                <h6 class="text-count_s">48</h6>
                                <span class="text-category_s">Products</span>
                            </div>
                        </div>
                        <!-- -------------  -->
                        <div class="row my-4 ms-5">
                            <div class="col">
                                <h6 class="text-count_s">34</h6>
                                <span class="text-category_s">Hits</span>
                            </div>
                            <div class="col">
                                <h6 class="text-count_s">4</h6>
                                <span class="text-category_s">Products Clicked</span>
                            </div>
                        </div>
                        <!-- -------------  -->
                        <div class="row my-4 ms-5">
                            <div class="col">
                                <h6 class="text-count_s">49</h6>
                                <span class="text-category_s">Sales</span>
                            </div>
                            <div class="col">
                                <h6 class="text-count_s">17</h6>
                                <span class="text-category_s">Products Shared</span>
                            </div>
                        </div>
                        <!-- -------------  -->
                    </div>
                </div>
            </div>
            <!-- 1st Section Summary End -->
            <div class="p-3 bg-white mt-4">
                <div class="row">
                    <div class="col table-responsive">
                        <!-- -------------  -->
                        <div class="row my-3">
                            <div class="col">
                                <h6 class="text-heading_s">Products</h6>
                            </div>
                            <div class="col d-flex justify-content-end">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                     class="bi bi-three-dots" viewBox="0 0 16 16">
                                    <path
                                        d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z"/>
                                </svg>
                            </div>
                        </div>
                        <table id="example" class="table table-striped" style="width:100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Categories</th>
                                <th>Clicks</th>
                                <th>Shares</th>
                                <th>Percentage</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>01</td>
                                <td><img class="img-fluid product-img_s" src="{{asset('images/profile-pic.png')}}" alt=""><span
                                        class="ms-3">Derma Soothe</span></td>
                                <td>Skin Care</td>
                                <td>12 Clicks</td>
                                <td>12 Shares</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>01</td>
                                <td><img class="img-fluid product-img_s" src="{{asset('images/profile-pic.png')}}" alt=""><span
                                        class="ms-3">Derma Soothe</span></td>
                                <td>Skin Care</td>
                                <td>12 Clicks</td>
                                <td>12 Shares</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>01</td>
                                <td><img class="img-fluid product-img_s" src="{{asset('images/profile-pic.png')}}" alt=""><span
                                        class="ms-3">Derma Soothe</span></td>
                                <td>Skin Care</td>
                                <td>12 Clicks</td>
                                <td>12 Shares</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>01</td>
                                <td><img class="img-fluid product-img_s" src="{{asset('images/profile-pic.png')}}" alt=""><span
                                        class="ms-3">Derma Soothe</span></td>
                                <td>Skin Care</td>
                                <td>12 Clicks</td>
                                <td>12 Shares</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>01</td>
                                <td><img class="img-fluid product-img_s" src="{{asset('images/profile-pic.png')}}" alt=""><span
                                        class="ms-3">Derma Soothe</span></td>
                                <td>Skin Care</td>
                                <td>12 Clicks</td>
                                <td>12 Shares</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>01</td>
                                <td><img class="img-fluid product-img_s" src="{{asset('images/profile-pic.png')}}" alt=""><span
                                        class="ms-3">Derma Soothe</span></td>
                                <td>Skin Care</td>
                                <td>12 Clicks</td>
                                <td>12 Shares</td>
                                <td>100%</td>
                            </tr>

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                    <h5 class="modal-title p-3 mb-4 fw-bold" id="exampleModalToggleLabel">Add Product</h5>

                    <div class="row gx-3 gx-xxl-5">
                        <div class="col">
                            <div class="p-3">
                                <div class="col text-center">
                                    <img id="education_certificate-d" src="{{asset('images/1.jpg')}}" class="upload_image-s" alt="">
                                    <h6 class="upload_thumbnail-s mt-1">Product Picture - 500 x 500px </h6>
                                    <div class="file-loading my-4">
                                        <label for="input-b9">
                                            <a class="btn btn-outline-primary upload_img-s my-4">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-lg" viewBox="0 0 16 16">
                                                    <path fill-rule="evenodd" d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                                                </svg>
                                                <span
                                                    class="ml-3 ">Upload Image</span></a>
                                        </label>
                                        <input id="input-b9" type='file' onchange="readURL2(this);" name="input-b9[]" class="d-none" multiple
                                               type="file">
                                    </div>
                                    <div id="kartik-file1-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="p-3">
                                <div class="col text-center">
                                    <img id="education_certificate-dd" src="{{asset('images/1.jpg')}}" class="upload_image-s" alt="">
                                    <h6 class="upload_thumbnail-s mt-1">Share Screen Picture - 250 x 250 px </h6>
                                    <div class="file-loading my-4">
                                        <label for="input-b99">
                                            <a class="btn btn-outline-primary upload_img-s my-4">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-lg" viewBox="0 0 16 16">
                                                    <path fill-rule="evenodd" d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                                                </svg>
                                                <span
                                                    class="ml-3 ">Upload Image</span></a>
                                        </label>
                                        <input id="input-b99" type='file' onchange="readURL3(this);" name="input-b99[]" class="d-none" multiple
                                               type="file">
                                    </div>
                                    <div id="kartik-file2-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col d-none d-lg-block">
                            <div class="p-3 mt-3">

                            </div>
                        </div>
                        <div class="col d-none d-lg-block">
                            <div class="p-3 mt-3">

                            </div>
                        </div>
                    </div>
                    <!-- Upload imges row end  -->
                    <div class="">
                        <div class="container row gx-lg-5">
                            <div class="col">
                                <div class="p-3">
                                    <form method="get" action="">
                                        <div class="col-md-12">
                                            <label for="inputEmail4" class="form-label my-2"> Name</label>
                                            <input type="text" class="form-control form-control-lg bg-white shadow-sm " id="inputEmail4" placeholder="Name">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="inputEmail4" class="form-label my-2 mt-5"> Category:</label>
                                            <input type="text" class="form-control form-control-lg bg-white shadow-sm " id="inputEmail4" placeholder="Category">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="inputEmail4" class="form-label my-2 mt-5"> Product Question:</label>
                                            <input type="text" class="form-control form-control-lg bg-white shadow-sm " id="inputEmail4" placeholder="Product Question">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="inputEmail4" class="form-label my-2 mt-5"> Product Answer:</label>
                                            <input type="text" class="form-control form-control-lg bg-white shadow-sm " id="inputEmail4" placeholder="Product Answer">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="inputEmail4" class="form-label my-3 mt-4"> Unique URL:</label>
                                            <input type="text" class="form-control form-control-lg bg-white shadow-sm " id="inputEmail4" placeholder="URl">
                                        </div>
                                </div>
                            </div>

                            <div class="col product-textarea_s">
                                <div class="p-3 ">
                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="form-label my-3 mt-4"> Description:</label>
                                        <textarea  class="form-control form-control-lg bg-white shadow-sm" rows="3" id="inputEmail4" placeholder="Description"></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="form-label my-3 mt-4"> Ingredients:</label>
                                        <textarea  class="form-control form-control-lg bg-white shadow-sm" rows="3" id="inputEmail4" placeholder="Ingredients"></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="form-label my-3 mt-4"> Allergens:</label>
                                        <textarea  class="form-control form-control-lg bg-white shadow-sm" rows="3" id="inputEmail4" placeholder="Allergens"></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="form-label my-3 mt-4"> Shipping:</label>
                                        <textarea  class="form-control form-control-lg bg-white shadow-sm" rows="3" id="inputEmail4" placeholder="Shipping"></textarea>
                                    </div>
                                </div>
                                <div class="col-12 justify-content-end d-flex my-4">
                                    <button type="button" class="btn btn-primary model-btn_s" data-bs-dismiss="modal" style="margin-right: 5px;">Close</button>
                                    <button type="submit" class="btn btn-primary model-btn_s ">Submit</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection
