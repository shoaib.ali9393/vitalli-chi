@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="container">
            <div class="row my-5">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 align-items-center d-flex">
                    <h5 class="fw-bold">Categories</h5>
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="d-flex justify-content-end">
                        <button type="button" class="btn btn-primary fw-bold rounded" data-bs-toggle="modal"
                                href="#addcategory" role="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                                 class="bi bi-plus-lg" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                      d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                            </svg>
                            &nbsp;Add Category
                        </button>
                    </div>
                </div>
            </div>
            @if(count($errors) > 0 )
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul class="p-0 m-0" style="list-style: none;">
                            <li class="mb-2">{{$error}}</li>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </ul>
                    </div>
                @endforeach
            @endif
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="    margin-top: 18px;
    width: 50%;">
                    {{ session()->get('success') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            <div class="p-3 bg-white mt-4">
                <div class="row">
                    <div class="col table-responsive">
                        <!-- -------------  -->
                        <div class="row my-3">
                            <div class="col">
                                <h6 class="text-heading_s">Category List</h6>
                            </div>

                        </div>
                        <table id="example" class="table table-striped" style="width:100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Products</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key=>$category)
                                @php
                                    $pathBanner="Null";
                                        if(isset($category->banner_image))
                                   {
                                    $pathBanner=asset('images/uploads/'.$category->banner_image);
                                         }
                                @endphp
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td><img class="img-fluid product-img_s mr-2"
                                             src="{{$pathBanner}}" alt="">{{$category->name}}</td>
                                    <td>{{$category->products_count}} Products</td>
                                    <td>
                                        <a onclick="addEditdata({{$category->id}});" data-bs-toggle="modal" href="#editcategory"><button type="button" class="btn btn-outline border-0"><img class="img-fluid cursor-pointer" src="{{asset('images/pencil.svg')}}" alt=""></button></a>
                                        <a onclick="adddeletedata({{$category->id}});" data-bs-toggle="modal" href="#deletecategory"><button type="button" class="btn btn-outline"> <img class="img-fluid cursor-pointer" src="{{asset('images/trash3.svg')}}" alt=""></button></a>
                                        <div class="status-bar_s form-check form-switch">
                                            <input class="form-check-input change_status" onchange="statusChange({{$category->id}})" type="checkbox"
                                                   name="change_status" id="changestatus{{$category->id}}" {{$category->status == \App\Models\Category::active ? "checked" : ""}}>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- -------- Add Category Modal ------    -->
    <div class="modal fade" id="addcategory" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    <h5 class="modal-title p-3 mb-4 fw-bold" id="exampleModalToggleLabel">Add Category</h5>
                    <div class="row gx-3 gx-xxl-5">
                        <form method="post" action="{{route('admin.add.category')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="col">
                                <div class="p-3">
                                    <div class="col text-center">
                                        <h6 class="upload_thumbnail-s">Upload Thumbnail</h6>
                                        <img id="category_pic" src=""
                                             class="upload_image-s @error('category_image') is-invalid @enderror"
                                             alt="">
                                        @error('category_image')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                        <div class="file-loading my-4">
                                            <label for="categoryimage">
                                                <a class="btn btn-outline-primary upload_img-s my-4">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                         fill="currentColor" class="bi bi-plus-lg" viewBox="0 0 16 16">
                                                        <path fill-rule="evenodd"
                                                              d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                                                    </svg>
                                                    <span class="ml-3">Upload Image</span></a>
                                            </label>
                                            <input id="categoryimage" type='file' onchange="loadCategoryPic(this);"
                                                   name="category_image" class="d-none"
                                                   type="file" required accept="image/png, image/gif, image/jpeg">
                                        </div>
                                        <div id="kartik-file1-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col me-5">
                                <div class="p-3 mt-3">
                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="form-label my-4">Category Name</label>
                                        <input type="text"
                                               class="form-control form-control-lg bg-white shadow-sm @error('category_name') is-invalid @enderror"
                                               name="category_name" placeholder="Category Name"
                                               value="{{old('category_name')}}" required>
                                        @error('category_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-12 justify-content-end d-flex my-4">
                                        <button type="button" class="btn btn-primary model-btn_s"
                                                data-bs-dismiss="modal" style="margin-right: 5px;">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary model-btn_s">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    {{--    Edit Category Model --}}
    <div class="modal fade" id="editcategory" aria-hidden="true" aria-labelledby="exampleModalToggleLabel"
         tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    <h5 class="modal-title p-3 mb-4 fw-bold" id="exampleModalToggleLabel">Edit Category</h5>
                    <div class="row gx-3 gx-xxl-5">
                        <form method="post" action="{{route('admin.update.category')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" id="category_id" name="id" value="">
                            <div class="col">
                                <div class="p-3">
                                    <div class="col text-center">
                                        <h6 class="upload_thumbnail-s">Upload Thumbnail</h6>
                                        <img id="edit_category_pic" src=""
                                             class="upload_image-s @error('edit_category_image') is-invalid @enderror"
                                             alt="">
                                        @error('edit_category_image')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                        <div class="file-loading my-4">
                                            <label for="edit_categoryimage">
                                                <a class="btn btn-outline-primary upload_img-s my-4">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                         fill="currentColor" class="bi bi-plus-lg" viewBox="0 0 16 16">
                                                        <path fill-rule="evenodd"
                                                              d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                                                    </svg>
                                                    <span class="ml-3">Upload Image</span></a>
                                            </label>
                                            <input id="edit_categoryimage" type='file'
                                                   onchange="editloadCategoryPic(this);"
                                                   name="edit_category_image" class="d-none"
                                                   type="file" accept="image/png, image/gif, image/jpeg">
                                        </div>
                                        <div id="kartik-file1-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col me-5">
                                <div class="p-3 mt-3">
                                    <div class="col-md-12">
                                        <label for="edit_category_name" class="form-label my-4">Category Name</label>
                                        <input type="text" id="edit_category_name"
                                               class="form-control form-control-lg bg-white shadow-sm @error('edit_category_name') is-invalid @enderror"
                                               name="edit_category_name" placeholder="Category Name"
                                               value="{{old('edit_category_name')}}" required>
                                        @error('edit_category_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-12 justify-content-end d-flex my-4">
                                        <button type="button" class="btn btn-primary model-btn_s"
                                                data-bs-dismiss="modal" style="margin-right: 5px;">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary model-btn_s">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    {{--    Delete Category Model--}}
    <div class="modal fade" id="deletecategory" aria-hidden="true" aria-labelledby="exampleModalToggleLabel"
         tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    <h5 class="modal-title p-3 mb-4 fw-bold" id="exampleModalToggleLabel">Edit Category</h5>
                    <div class="row gx-3 gx-xxl-5">
                        <form method="post" action="{{route('admin.delete.category')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" id="delete_id" name="id" value="">
                            <span>If You Delete! All Products which are connected to this Category also Delete!</span>
                            <div class="col me-5">
                                <div class="p-3 mt-3">
                                    <div class="col-12 justify-content-end d-flex my-4">
                                        <button type="button" class="btn btn-primary model-btn_s"
                                                data-bs-dismiss="modal" style="margin-right: 5px;">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary model-btn_s">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        function loadCategoryPic(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#category_pic').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function editloadCategoryPic(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#edit_category_pic').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function addEditdata(id) {
            $('#category_id').val(id);
            $.ajax({
                type: "GET",
                url: "{{route('ajax.category.data')}}",
                data: {
                    "id": id,
                },
                success: function (data) {
                    var img_url ="Null";
                    $('#edit_category_name').val(data.name);
                    if(data.banner_image != null && data.banner_image != "Null" ) {
                        img_url = {!! json_encode(url('/')) !!}+"/images/uploads/" + data.banner_image;
                    }
                    $('#edit_category_pic').attr('src', img_url);
                }
            });
        }
        function adddeletedata(id) {
            $('#delete_id').val(id);
        }


        function statusChange(id) {
            window.location.href = "{{ route('admin.update.status.category')}}"+"/"+id;
        }

    </script>
@endsection
