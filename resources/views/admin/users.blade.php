@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="container">
            <div class="row my-5">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <h5 class="fw-bold">Users</h5>
                </div>

            </div>
            <div class="p-3 bg-white mt-4">
                <div class="row">
                    <div class="col table-responsive">
                        <!-- -------------  -->
{{--                        <div class="row my-3">--}}
{{--                            <div class="col">--}}
{{--                                <h6 class="text-heading_s">User List</h6>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <table id="example" class="table table-striped" style="width:100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>User URl</th>
                                <th>Products Click</th>
                                <th>Products Shared</th>
                                <th>Commissions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row" class="table-count_s">01</th>
                                <td>User/URL</td>
                                <td>12</td>
                                <td>12 </td>
                                <td>2$</td>
                            </tr>
                            <tr>
                                <th scope="row" class="table-count_s">02</th>
                                <td>User/URL</td>
                                <td>17</td>
                                <td>10</td>
                                <td>2$</td>
                            </tr>
                            <tr>
                                <th scope="row" class="table-count_s">03</th>
                                <td>User/URL</td>
                                <td>15</td>
                                <td>16 </td>
                                <td>2$</td>
                            </tr>
                            <tr>
                                <th scope="row" class="table-count_s">04</th>
                                <td>User/URL</td>
                                <td>15</td>
                                <td>10</td>
                                <td>2$</td>
                            </tr>
                            <tr>
                                <th scope="row" class="table-count_s">05</th>
                                <td>User/URL</td>
                                <td>15</td>
                                <td>16 </td>
                                <td>2$</td>
                            </tr><tr>
                                <th scope="row" class="table-count_s">06</th>
                                <td>User/URL</td>
                                <td>15</td>
                                <td>16 </td>
                                <td>2$</td>
                            </tr>
                            <tr>
                                <th scope="row" class="table-count_s">07</th>
                                <td>User/URL</td>
                                <td>15</td>
                                <td>10</td>
                                <td>2$</td>
                            </tr><tr>
                                <th scope="row" class="table-count_s">08</th>
                                <td>User/URL</td>
                                <td>15</td>
                                <td>10</td>
                                <td>2$</td>
                            </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
