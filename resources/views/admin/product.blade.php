@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="container">
            <div class="row my-5">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 align-items-center d-flex">
                    <h5 class="fw-bold">Products</h5>
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="d-flex justify-content-end">
                        <button type="button" class="btn btn-primary fw-bold rounded" data-bs-toggle="modal"
                                href="#addproductModalToggle" role="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                                 class="bi bi-plus-lg" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                      d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                            </svg>
                            &nbsp;Add Product
                        </button>
                    </div>
                </div>
            </div>
            @if(count($errors) > 0 )
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul class="p-0 m-0" style="list-style: none;">
                            <li class="mb-2">{{$error}}</li>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </ul>
                    </div>
                @endforeach
            @endif
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="    margin-top: 18px;
    width: 50%;">
                    {{ session()->get('success') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            <div class="p-3 bg-white mt-4">
                <div class="row">
                    <div class="col table-responsive">
                        <!-- -------------  -->
                        <div class="row my-3">
                            <div class="col">
                                <h6 class="text-heading_s">Product List</h6>
                            </div>
                        </div>
                        <table id="example" class="table table-striped" style="width:100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Banner Image</th>
                                <th>Product Image</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Price</th>
                                <th>Unique URL</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($productData as $key=>$products)
                                @php
                                    $pathBanner="Null";
                                    $images="Null";
                                        if(isset($products->banner_image))
                                   {
                                    $pathBanner=asset('images/uploads/'.$products->banner_image);
                                         }
                                        if(count($products->productImages) > 0){
                                        $images=asset('images/uploads/'.$products->productImages[0]->image);
                                        }
                                @endphp
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td><img class="img-fluid product-img_s" src="{{$pathBanner}}" alt="">
                                    </td>
                                    <td><img class="img-fluid product-img_s" src="{{$images}}" alt="">
                                    </td>
                                    <td>{{$products->name}}</td>
                                    <td>{{$products->categorys->name}}</td>
                                    <td>{{$products->price}}</td>
                                    <td>{{isset($products->unique_url) ? $products->unique_url : "Not Set Yet" }}</td>
                                    <td>
                                        <a  onclick="addEditdata({{$products->id}});" data-bs-toggle="modal" href="#editproductModal"><button type="button" class="btn btn-outline border-0"><img class="img-fluid cursor-pointer" src="{{asset('images/pencil.svg')}}" alt=""></button></a>
                                        <a onclick="adddeleteid({{$products->id}});" data-bs-toggle="modal" href="#deleteproduct"><button type="button" class="btn btn-outline"> <img class="img-fluid cursor-pointer" src="{{asset('images/trash3.svg')}}" alt=""></button></a>
                                        <div class="status-bar_s form-check form-switch">
                                            <input class="form-check-input change_status" onchange="statusChange({{$products->id}})" type="checkbox"
                                                   name="change_status" id="changestatus{{$products->id}}" {{$products->status == \App\Models\Product::active ? "checked" : ""}}>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--    Add product Model--}}
    <div class="modal fade" id="addproductModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel"
         tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    <h5 class="modal-title p-3 mb-4 fw-bold" id="exampleModalToggleLabel">Add Product</h5>
                    <form method="post" action="{{route('admin.add.product')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row gx-3 gx-xxl-5">
                            <div class="col">
                                <div class="p-3">
                                    <div class="col text-center">
                                        <img id="product_pic_banner" src=""
                                             class="upload_image-s @error('product_banner') is-invalid @enderror"
                                             alt="">
                                        @error('product_banner')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                        <h6 class="upload_thumbnail-s mt-1">Banner Image - 500 x 500px </h6>
                                        <div class="file-loading my-4">
                                            <label for="product_image_banner">
                                                <a class="btn btn-outline-primary upload_img-s my-4">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                         fill="currentColor" class="bi bi-plus-lg" viewBox="0 0 16 16">
                                                        <path fill-rule="evenodd"
                                                              d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                                                    </svg>
                                                    <span
                                                        class="ml-3 ">Upload Image</span></a>
                                            </label>
                                            <input id="product_image_banner" type='file'
                                                   onchange="loadproductBanner(this);" name="product_banner"
                                                   class="d-none"
                                                   accept="image/png, image/gif, image/jpeg" required>
                                        </div>
                                        <div id="kartik-file1-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="p-3">
                                    <div class="col text-center">
                                        <img id="product_pic" src=""
                                             class="upload_image-s @error('product_image') is-invalid @enderror" alt="">
                                        @error('product_image')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                        <h6 class="upload_thumbnail-s mt-1">Share Screen Picture - 250 x 250 px </h6>
                                        <div class="file-loading my-4">
                                            <label for="product_image">
                                                <a class="btn btn-outline-primary upload_img-s my-4">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                         fill="currentColor" class="bi bi-plus-lg" viewBox="0 0 16 16">
                                                        <path fill-rule="evenodd"
                                                              d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                                                    </svg>
                                                    <span
                                                        class="ml-3 ">Upload Image</span></a>
                                            </label>
                                            <input id="product_image" type='file' onchange="loadproductPic(this);"
                                                   name="product_image" class="d-none"
                                                   accept="image/png, image/gif, image/jpeg" required>
                                        </div>
                                        <div id="kartik-file2-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col d-none d-lg-block">
                                <div class="p-3 mt-3">

                                </div>
                            </div>
                            <div class="col d-none d-lg-block">
                                <div class="p-3 mt-3">

                                </div>
                            </div>
                        </div>
                        <!-- Upload imges row end  -->
                        <div class="">
                            <div class="container row gx-lg-5">
                                <div class="col">
                                    <div class="p-3">
                                        <div class="col-md-12">
                                            <label for="product_name" class="form-label my-2"> Name</label>
                                            <input type="text"
                                                   class="form-control form-control-lg bg-white shadow-sm @error('product_name') is-invalid @enderror"
                                                   id="product_name" name="product_name" placeholder="Name"
                                                   value="{{old('product_name')}}" required>
                                            @error('product_name')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="product_price" class="form-label my-2">Price</label>
                                            <input type="text"
                                                   class="form-control form-control-lg bg-white shadow-sm @error('price') is-invalid @enderror"
                                                   id="price" name="price" placeholder="Price"
                                                   value="{{old('price')}}" required>
                                            @error('price')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="product_category" class="form-label my-2 mt-5">
                                                Category:</label>
                                            <select id="product_category" name="product_category"
                                                    class="form-select @error('product_category') is-invalid @enderror"
                                                    aria-label="Default select example" required>
                                                <option selected value="">Select Category</option>
                                                @foreach(\App\Models\Category::all() as $category_data)
                                                    <option
                                                        value="{{$category_data->id}}">{{$category_data->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('product_category')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="product_question" class="form-label my-2 mt-5"> Product
                                                Question:</label>
                                            <input type="text"
                                                   class="form-control form-control-lg bg-white shadow-sm @error('product_question') is-invalid @enderror"
                                                   id="product_question" name="product_question"
                                                   placeholder="Product Question" value="{{old('product_question')}}">
                                            @error('product_question')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="product_answer" class="form-label my-2 mt-5"> Product
                                                Answer:</label>
                                            <input type="text"
                                                   class="form-control form-control-lg bg-white shadow-sm @error('product_answer') is-invalid @enderror"
                                                   id="product_answer" name="product_answer"
                                                   placeholder="Product Answer" value="{{old('product_answer')}}">
                                            @error('product_answer')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col product-textarea_s">
                                    <div class="p-3 ">
                                        <div class="col-md-12">
                                            <label for="product_description" class="form-label my-3 mt-4">
                                                Description:</label>
                                            <textarea
                                                class="form-control form-control-lg bg-white shadow-sm @error('product_description') is-invalid @enderror"
                                                rows="3" id="product_description" name="product_description"
                                                placeholder="Description"> {{old('product_description')}}</textarea>
                                            @error('product_description')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="product_ingredients" class="form-label my-3 mt-4">
                                                Ingredients:</label>
                                            <textarea
                                                class="form-control form-control-lg bg-white shadow-sm @error('product_ingredients') is-invalid @enderror"
                                                rows="3" id="product_ingredients" name="product_ingredients"
                                                placeholder="Ingredients">{{old('product_ingredients')}}</textarea>
                                            @error('product_ingredients')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="product_allergens" class="form-label my-3 mt-4">
                                                Allergens:</label>
                                            <textarea
                                                class="form-control form-control-lg bg-white shadow-sm @error('product_allergens') is-invalid @enderror"
                                                rows="3" id="product_allergens" name="product_allergens"
                                                placeholder="Allergens">{{old('product_allergens')}}</textarea>
                                            @error('product_allergens')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="product_shipping" class="form-label my-3 mt-4">
                                                Shipping:</label>
                                            <textarea
                                                class="form-control form-control-lg bg-white shadow-sm @error('product_shipping') is-invalid @enderror"
                                                rows="3" id="product_shipping" name="product_shipping"
                                                placeholder="Shipping">{{old('product_shipping')}}</textarea>
                                            @error('product_shipping')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12 justify-content-end d-flex my-4">
                                        <button type="button" class="btn btn-primary model-btn_s"
                                                data-bs-dismiss="modal" style="margin-right: 5px;">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary model-btn_s ">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    {{--    Delete product Model--}}
    <div class="modal fade" id="deleteproduct" aria-hidden="true" aria-labelledby="exampleModalToggleLabel"
         tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    <h5 class="modal-title p-3 mb-4 fw-bold" id="exampleModalToggleLabel">Delete Product</h5>
                    <div class="row gx-3 gx-xxl-5">
                        <form method="post" action="{{route('admin.delete.product')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" id="deleteproduct_id" name="id" value="">
                            <span>Are You sure you want to Delete this Product!</span>
                            <div class="col me-5">
                                <div class="p-3 mt-3">
                                    <div class="col-12 justify-content-end d-flex my-4">
                                        <button type="button" class="btn btn-primary model-btn_s"
                                                data-bs-dismiss="modal" style="margin-right: 5px;">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary model-btn_s">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    {{--    Edit product Model--}}
    <div class="modal fade" id="editproductModal" aria-hidden="true" aria-labelledby="exampleModalToggleLabel"
         tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    <h5 class="modal-title p-3 mb-4 fw-bold" id="exampleModalToggleLabel">Edit Product</h5>
                    <form method="post" action="{{route('admin.update.product')}}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" id="edit_product_id" name="id" value="">
                        <div class="row gx-3 gx-xxl-5">
                            <div class="col">
                                <div class="p-3">
                                    <div class="col text-center">
                                        <img id="edit_product_pic_banner" src=""
                                             class="upload_image-s @error('edit_product_banner') is-invalid @enderror"
                                             alt="">
                                        @error('edit_product_banner')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                        <h6 class="upload_thumbnail-s mt-1">Banner Image - 500 x 500px </h6>
                                        <div class="file-loading my-4">
                                            <label for="edit_product_image_banner">
                                                <a class="btn btn-outline-primary upload_img-s my-4">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                         fill="currentColor" class="bi bi-plus-lg" viewBox="0 0 16 16">
                                                        <path fill-rule="evenodd"
                                                              d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                                                    </svg>
                                                    <span
                                                        class="ml-3 ">Upload Image</span></a>
                                            </label>
                                            <input id="edit_product_image_banner" type='file'
                                                   onchange="editloadproductBanner(this);" name="edit_product_banner"
                                                   class="d-none"
                                                   accept="image/png, image/gif, image/jpeg">
                                        </div>
                                        <div id="kartik-file1-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="p-3">
                                    <div class="col text-center">
                                        <img id="edit_product_pic" src=""
                                             class="upload_image-s @error('edit_product_image') is-invalid @enderror"
                                             alt="">
                                        @error('edit_product_image')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                        <h6 class="upload_thumbnail-s mt-1">Share Screen Picture - 250 x 250 px </h6>
                                        <div class="file-loading my-4">
                                            <label for="edit_product_image">
                                                <a class="btn btn-outline-primary upload_img-s my-4">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                         fill="currentColor" class="bi bi-plus-lg" viewBox="0 0 16 16">
                                                        <path fill-rule="evenodd"
                                                              d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                                                    </svg>
                                                    <span
                                                        class="ml-3 ">Upload Image</span></a>
                                            </label>
                                            <input id="edit_product_image" type='file'
                                                   onchange="editloadproductPic(this);"
                                                   name="edit_product_image" class="d-none"
                                                   accept="image/png, image/gif, image/jpeg">
                                        </div>
                                        <div id="kartik-file2-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col d-none d-lg-block">
                                <div class="p-3 mt-3">

                                </div>
                            </div>
                            <div class="col d-none d-lg-block">
                                <div class="p-3 mt-3">

                                </div>
                            </div>
                        </div>
                        <!-- Upload imges row end  -->
                        <div class="">
                            <div class="container row gx-lg-5">
                                <div class="col">
                                    <div class="p-3">
                                        <div class="col-md-12">
                                            <label for="edit_product_name" class="form-label my-2"> Name</label>
                                            <input type="text"
                                                   class="form-control form-control-lg bg-white shadow-sm @error('edit_product_name') is-invalid @enderror"
                                                   id="edit_product_name" name="edit_product_name" placeholder="Name"
                                                   required>
                                            @error('edit_product_name')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="edit_price" class="form-label my-2">price</label>
                                            <input type="text"
                                                   class="form-control form-control-lg bg-white shadow-sm @error('edit_price') is-invalid @enderror"
                                                   id="edit_price" name="edit_price" placeholder="Price"
                                                   required>
                                            @error('edit_price')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="edit_product_category" class="form-label my-2 mt-5">
                                                Category:</label>
                                            <select id="edit_product_category" name="edit_product_category"
                                                    class="form-select @error('edit_product_category') is-invalid @enderror"
                                                    aria-label="Default select example" required>
                                                <option selected value="">Select Category</option>
                                                @foreach(\App\Models\Category::all() as $category_data)
                                                    <option
                                                        value="{{$category_data->id}}">{{$category_data->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('edit_product_category')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="edit_product_question" class="form-label my-2 mt-5"> Product
                                                Question:</label>
                                            <input type="text"
                                                   class="form-control form-control-lg bg-white shadow-sm @error('edit_product_question') is-invalid @enderror"
                                                   id="edit_product_question" name="edit_product_question"
                                                   placeholder="Product Question">
                                            @error('edit_product_question')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="edit_product_answer" class="form-label my-2 mt-5"> Product
                                                Answer:</label>
                                            <input type="text"
                                                   class="form-control form-control-lg bg-white shadow-sm @error('edit_product_answer') is-invalid @enderror"
                                                   id="edit_product_answer" name="edit_product_answer"
                                                   placeholder="Product Answer">
                                            @error('edit_product_answer')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col product-textarea_s">
                                    <div class="p-3 ">
                                        <div class="col-md-12">
                                            <label for="edit_product_description" class="form-label my-3 mt-4">
                                                Description:</label>
                                            <textarea
                                                class="form-control form-control-lg bg-white shadow-sm @error('edit_product_description') is-invalid @enderror"
                                                rows="3" id="edit_product_description" name="edit_product_description"
                                                placeholder="Description"></textarea>
                                            @error('edit_product_description')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="edit_product_ingredients" class="form-label my-3 mt-4">
                                                Ingredients:</label>
                                            <textarea
                                                class="form-control form-control-lg bg-white shadow-sm @error('edit_product_ingredients') is-invalid @enderror"
                                                rows="3" id="edit_product_ingredients" name="edit_product_ingredients"
                                                placeholder="Ingredients"></textarea>
                                            @error('edit_product_ingredients')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="edit_product_allergens" class="form-label my-3 mt-4">
                                                Allergens:</label>
                                            <textarea
                                                class="form-control form-control-lg bg-white shadow-sm @error('edit_product_allergens') is-invalid @enderror"
                                                rows="3" id="edit_product_allergens" name="edit_product_allergens"
                                                placeholder="Allergens"></textarea>
                                            @error('edit_product_allergens')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="edit_product_shipping" class="form-label my-3 mt-4">
                                                Shipping:</label>
                                            <textarea
                                                class="form-control form-control-lg bg-white shadow-sm @error('edit_product_shipping') is-invalid @enderror"
                                                rows="3" id="edit_product_shipping" name="edit_product_shipping"
                                                placeholder="Shipping"></textarea>
                                            @error('edit_product_shipping')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12 justify-content-end d-flex my-4">
                                        <button type="button" class="btn btn-primary model-btn_s"
                                                data-bs-dismiss="modal" style="margin-right: 5px;">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary model-btn_s ">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        function loadproductBanner(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#product_pic_banner').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function loadproductPic(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#product_pic').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function editloadproductBanner(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#edit_product_pic_banner').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function editloadproductPic(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#edit_product_pic').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function adddeleteid(id) {
            $('#deleteproduct_id').val(id);
        }

        function addEditdata(id) {
            $('#edit_product_id').val(id);
            $.ajax({
                type: "GET",
                url: "{{route('ajax.product.data')}}",
                data: {
                    "id": id,
                },
                success: function (data) {
                    var img_url="Null";
                    var img_url_banner="Null";
                    $('#edit_product_name').val(data.name);
                    $('#edit_price').val(data.price);
                    if(data.banner_image != null && data.banner_image != "Null" ) {
                        img_url_banner = {!! json_encode(url('/')) !!}+"/images/uploads/" + data.banner_image;
                    }
                    if(data.product_images[0] != null && data.product_images[0] != "Null" ) {
                        img_url = {!! json_encode(url('/')) !!}+"/images/uploads/" + data.product_images[0].image;
                    }
                    $('#edit_product_pic_banner').attr('src', img_url_banner);
                    $('#edit_product_pic').attr('src', img_url);
                    $('#edit_product_allergens').val(data.allergens);
                    $('#edit_product_description').val(data.description);
                    $('#edit_product_answer').val(data.product_answer);
                    $('#edit_product_question').val(data.product_question);
                    $('#edit_product_ingredients').val(data.ingredients);
                    $('#edit_product_shipping').val(data.shipping);
                    $("#edit_product_category").val(data.category_id)
                        .find("option[value=" + data.category_id + "]").attr('selected', true);

                }
            });
        }

        function statusChange(id) {
            window.location.href = "{{ route('admin.update.status.product')}}"+"/"+id;
        }
    </script>
@endsection
