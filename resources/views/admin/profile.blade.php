@extends('layouts.admin')
@section('content')
    <div class="container-fluid">

        <div class="container">
            <div class="row my-5">
                <form method="post" action="{{route('admin.profile.save')}}" enctype="multipart/form-data">
                    @csrf
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert" style="    margin-top: 18px;
    ">
                            {{ session()->get('success') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                <div class="col d-flex justify-content-center">
                    <div class="card" style="width: 25rem">
                        <div class="card-body">
                            <div class="col text-center">
                                <div class="file-loading my-4">
                                    <input
                                        id="input_profile_image"
                                        type="file"
                                        onchange="readimg(this);"
                                        name="profile_image"
                                        class="d-none"
                                        multiple
                                    />
                                </div>
                                <div id="kartik-file1-errors"></div>
                            </div>

                            <div class="text-center my-3">
                                <img
                                    id="profile_image"
                                    src="{{($user->image == null) ? asset('images/user_img.png') : asset('images/uploads/'.$user->image)}}"
                                    class="rounded-circle position-relative"
                                    width="100"
                                    alt=""
                                />
                                <label for="input_profile_image" class="position-absolute edit-pic_s">
                                    <a class="btn btn-outline border-0 upload_img-s my-4">
                                        <img class="img-fluid cursor-pointer" src="{{asset('images/pencil.svg')}}" alt="">
                                    </a>
                                </label>
                            </div>

                            <div class="mb-3">
                                <label for="name" class="form-label"
                                >Full Name</label
                                >
                                <input
                                    type="text"
                                    class="form-control form-control-lg"
                                    id="name"
                                    name="name"
                                    value="{{$user->name}}"
                                    placeholder="Name"
                                    required
                                />
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label"
                                >Email</label
                                >
                                <input
                                    type="email"
                                    class="form-control form-control-lg"
                                    id="email"
                                    name="email"
                                    value="{{$user->email}}"
                                    placeholder="Email" disabled
                                />
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label"
                                >Change Password</label
                                >
                                <input
                                    type="password"
                                    class="form-control form-control-lg @error('password') is-invalid @enderror"
                                    id="password"
                                    name="password"
                                    placeholder="******"
                                />
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="password_confirm" class="form-label"
                                >Re-enter Password</label
                                >
                                <input
                                    type="password"
                                    class="form-control form-control-lg"
                                    id="password_confirm"
                                    name="password_confirmation"
                                    placeholder="******"
                                />
                            </div>
                            <div class="col-auto text-center my-3 mt-5">
                                <button type="submit" class="btn btn-primary mb-3">
                                    Save Changes
                                </button>
                            </div>
                        </div>
                    </div>

                </div>

                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        function readimg(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#profile_image')
                        .attr('src', e.target.result);
                };
            reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
