<html>
<head>
    @include('partials.admin.head')
</head>
<body>
<div class="d-flex" id="wrapper">
    @include('partials.admin.sidebar')
    <div id="page-content-wrapper">
        @include('partials.admin.header')

        @yield('content')
    </div>

</div>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src = "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    integrity = "sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
    crossorigin = "anonymous" ></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

<script>
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $(document).ready(function () {
        $('#example').DataTable();
    });

    // $(document).ready(function() {
    //     $("#input-b99").fileinput({
    //         showPreview: false,
    //         showUpload: false,
    //         elErrorContainer: "#kartik-file2-errors",
    //         allowedFileExtensions: ["jpg", "png", "gif"],
    //         //uploadUrl: '/site/file-upload-single '
    //     });
    // });
    //
    // $(document).ready(function() {
    //     $("#input-b9").fileinput({
    //         showPreview: false,
    //         showUpload: false,
    //         elErrorContainer: "#kartik-file1-errors",
    //         allowedFileExtensions: ["jpg", "png", "gif"],
    //
    //     });
    // });
</script>
@yield('js')

<body>
<html>
