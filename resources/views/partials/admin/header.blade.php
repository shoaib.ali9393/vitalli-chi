<nav class="navbar navbar-expand-lg navbar-light border-bottom navbar_header-s">
    <svg xmlns="http://www.w3.org/2000/svg" id="menu-toggle" width="20" height="20" fill="currentColor"
         class="bi bi-border-width sidebar-s ms-2" viewBox="0 0 16 16">
        <path
            d="M0 3.5A.5.5 0 0 1 .5 3h15a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5H.5a.5.5 0 0 1-.5-.5v-2zm0 5A.5.5 0 0 1 .5 8h15a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H.5a.5.5 0 0 1-.5-.5v-1zm0 4a.5.5 0 0 1 .5-.5h15a.5.5 0 0 1 0 1H.5a.5.5 0 0 1-.5-.5z"/>
    </svg>
    <!-- <img src="assets/sidebar_line.png d-none d-lg-block" id="menu-toggle" class="filter-green" width="20" alt="">&nbsp;&nbsp;&nbsp; -->
    <form class="d-flex ms-3 d-none d-lg-block">
        <!-- <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search mt-2" viewBox="0 0 16 16">
            <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
        </svg>
      <input class="form-control me-2 nav-search_s" type="search" placeholder="Search Something..." aria-label="Search"> -->
        <div class="input-group">
                        <span class="input-group-text nav-search_s bg-white" id="basic-addon1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                 class="bi bi-search " viewBox="0 0 16 16">
                            <path
                                d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                            </svg>
                         </span>
            <input type="text" class="form-control nav-search_s" placeholder="Search Something..." aria-label="Username"
                   aria-describedby="basic-addon1">
        </div>
    </form>
    <!-- <button class="navbar-toggler " type="button" data-toggle="collapse"  data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span
            class="navbar-toggler-icon"></span></button> -->
    <div class=" navbar-collapse d-flex" id="navbarSupportedContent">
        <ul class="navbar-nav ms-auto  mt-lg-0 me-4 d-flex flex-row profile-pic_s">
            <li class="nav-item mx-lg-5 mt-1">
                <a class="nav-link" href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                         class="bi bi-bell" viewBox="0 0 16 16">
                        <path
                            d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zM8 1.918l-.797.161A4.002 4.002 0 0 0 4 6c0 .628-.134 2.197-.459 3.742-.16.767-.376 1.566-.663 2.258h10.244c-.287-.692-.502-1.49-.663-2.258C12.134 8.197 12 6.628 12 6a4.002 4.002 0 0 0-3.203-3.92L8 1.917zM14.22 12c.223.447.481.801.78 1H1c.299-.199.557-.553.78-1C2.68 10.2 3 6.88 3 6c0-2.42 1.72-4.44 4.005-4.901a1 1 0 1 1 1.99 0A5.002 5.002 0 0 1 13 6c0 .88.32 4.2 1.22 6z"/>
                    </svg>
                </a>
            </li>
            <!-- <li> -->
{{--            <li class="nav-item"><img src="{{($user->image == null) ? asset('images/user_img.png') : asset('images/uploads/'.\Illuminate\Support\Facades\Auth::user()->image)}}" class="rounded-circle img-thumbnail"--}}
{{--                                      alt=""></li>--}}
            <li class="nav-item"><img src="{{asset('images/user_img.png')}}" class="rounded-circle img-thumbnail"
                                      alt=""></li>

            <li class="nav-item dropdown ml-auto">
                <a class="nav-link dropdown-toggle nav_dropdown-s pt-3" href="#" id="navbarDropdown" role="button"
                   data-bs-toggle="dropdown" aria-expanded="false">

                </a>
                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="{{route('admin.profile')}}">Settings</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" class="dropdown-item" >Log Out</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</nav>
